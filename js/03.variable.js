// 03.Data and Variables Part 2 
// 

	// var sum = 3 + 4;
	// var diff = 3 - 4;

	var sum      = 3 + 4,
		diff     = 3 - 4,
		product  = 3 * 4,
		quotient = 3 / 4;

	// alert(product);

	var math = 3 + 4 * (3 - 4);

	// alert(math);

	var hello = "Hello",
		world = " World!";

	// alert(hello + world);

	// ParseInt / ParseFloat

	var num = "7";
	var num2 = "7.5";

	var foo = 1 + 2 + "7" + 10;
	var foo2 = 1 + 2 + parseInt(num, 10) + 10;
	var foo3 = 1 + 2 + parseInt(num2, 10) + 10;
	var foo4 = 1 + 2 + parseFloat(num2, 10) + 10;

	alert(foo + "," + foo2 + "," + foo3 + "," + foo4);