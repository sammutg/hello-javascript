// 06.objects.mp4 
// 

	var 	num = 8,
			// str = num + ""; // "8"
			/* egale */ 
			str = num.toString(); // "8"

	alert(num + " , " + str);
	alert(typeof str + " , " + typeof num);


// Create an object

	var obj    = new Object(); // Object constructor
	var string = new String("Hello, World!"); // String constructor
	/* egale */
	var string = "Hello, World!"; // String constructor


// 01. Old Fashion Create an object with methods.

	// var person = new Object();
	// 	person.firstName   = "guillaume";
	// 	person.lastName    = "sammut";
	// 	person.getFullName = function() {
	// 		return person.firstName + " " + person.lastName;
	// 		/* egale */
	// 		return this.firstName + " " + this.lastName;
	// 	};
	// 	/* egale */
	// 	// var getFullName = function() {
	// 	// };

	// 	alert( person.getFullName() );


// 02. New Fashion Create an object with methods.

	// var person = { 
	// 	firstName : "Guillaume", 
	// 	lastName : "Sammut", 
	// 	getFullName : function() {
	// 		return this.firstName + " " + this.lastName;
	// 	}
	// };

	// alert( person.getFullName() );



// Exercices 

var homme = {
	nom : "sammut",
	prenom	: "guillaume",
	nomPrenom : function() {
		return (this.nom + " " + this.prenom).toUpperCase();
	}
}

alert( homme.nomPrenom() );