// 04.Functions.mp4 
// 

// set one

	var foo = 3 + 4;
		foo = foo + 1;
		foo = foo * 3;

	// alert(foo);

// set two

	var bar = 3 + 4;
		bar = bar + 1;
		bar = bar * 3;

	// alert(bar);

// function 

	// function doSomething(paramOne, paramTwo) {
	// 	paramOne = paramOne + 4,
	// 	paramOne = paramOne + 1;
	// 	paramOne = paramOne * 3;

	// 	return	paramOne * paramTwo;
	// }

	// var bar2 = doSomething(3, 4);
	// alert(bar2);


// anomymous function 

	var doSomething = function(paramOne, paramTwo, fn) {
	// function doSomething(paramOne, paramTwo) {
		paramOne = paramOne + 3,
		paramOne = paramOne + 1;
		paramOne = paramOne * 8;

		return	fn(paramOne, paramTwo);
	};

	function sum(paramOne, paramTwo) {
		return (paramOne + paramTwo);
	}

	function product(paramOne, paramTwo) {
		return	(paramOne * paramTwo);
	}

	var foo = doSomething(2, 2, sum);
	var bar = doSomething(3, 2, product);

	alert(foo);
	alert(bar);