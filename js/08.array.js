// 08.Array 
// 

// A. Old Syntax

	var foo = new Array( "Hello", 3, true);
	var length = foo.length;
	var value = foo[0];

	alert(value);


// B. New Syntax

	var foo 			= ["Hello", 3, true];
		foo[3]          = "test";
		foo[4]          = 15;
		//foo[foo.length] = "new value";
		foo.push("new value again");

	alert(foo[5]);


// C. 

	var names 	= ["Guillaume", "Sammut"],
		names2 	= ["Rouzier", "Sophie"];



	var joined   = people.join(" / ");
	var reversed = people.reverse();
	var sorted   = people.sort();


	alert(sorted);