// 05.scope.mp4 
// 

// Global Scope

	var globalVar = "This a global var";

	var globalFunction = function(paramOne) {

		var localVar = "This is a local var"

		var localFunction = function() {
			var localVar = "This is a new local var"; // Utilise cette variable dans cette fonction / overide la varialble du dessus.
			alert(localVar);
			// var superLocalVar = "Hello World!";
			// alert(globalVar);
			// alert(paramOne);
			// alert(localVar);
			// alert(superLocalVar);
			// alert(foo);
		};

		localFunction(2);
		// alert(superLocalVar);
		alert(localVar);

		globalVar = "value has been modified";
		// alert(localVar);
	};

	globalFunction(2);

	// alert(globalVar);
	// alert(localVar);
	// alert(superLocalVar);